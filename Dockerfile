FROM ubuntu:20.04 AS builder

RUN apt-get update \
    && apt-get install -y git

# Clone webots. Increase the number being echoed to force re-clone to
# latest version of branch
RUN echo 1 \
    && mkdir -p /opt/webots \
    && git clone \
    --branch release \
    --depth 1 \
    --recurse-submodules \
    https://github.com/RoboCup-Humanoid-TC/webots.git \
    /opt/webots/webots

WORKDIR /opt/webots/webots

# Install compilation dependencies
RUN DEBIAN_FRONTEND=noninteractive scripts/install/linux_compilation_dependencies.sh
RUN apt-get install -y xvfb python3-dev

# Build Webots
RUN make -j$(nproc)

# Build RoboCup controller
ENV WEBOTS_HOME=/opt/webots/webots

RUN apt-get install -y python3-pip

RUN pip3 install -r $WEBOTS_HOME/projects/samples/contests/robocup/controllers/referee/requirements.txt

RUN apt-get install -y protobuf-compiler libprotobuf-dev libjpeg9-dev

RUN cd projects/samples/contests/robocup && make -j$(nproc)

# Build game controller
RUN apt-get install -y ant default-jdk
RUN git clone https://github.com/RoboCup-Humanoid-TC/GameController /opt/webots/gamecontroller

WORKDIR /opt/webots/gamecontroller

RUN ant

# The volume will be mounted read-only, so create a symbolic link to a
# writable location where the RoboCup controller will try to log to
RUN ln -s /tmp/referee_log.txt $WEBOTS_HOME/projects/samples/contests/robocup/controllers/referee/log.txt
RUN ln -s /tmp/bouncing_log.txt $WEBOTS_HOME/projects/samples/contests/robocup/controllers/referee/bouncing_log.txt
RUN rm /opt/webots/gamecontroller/build/jar/config/hl_sim_kid/teams.cfg
RUN ln -s /tmp/teams.cfg /opt/webots/gamecontroller/build/jar/config/hl_sim_kid/teams.cfg

# Create actual image
FROM ubuntu:20.04

COPY --from=builder /opt/webots /opt/webots

COPY env.sh /opt/webots/.env.sh
COPY adeinit /opt/webots/.adeinit

VOLUME /opt/webots

CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]

