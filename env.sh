export JAVA_HOME=/usr/lib/jvm/default-java

export WEBOTS_DISABLE_SAVE_SCREEN_PERSPECTIVE_ON_CLOSE=1
export WEBOTS_ALLOW_MODIFY_INSTALLATION=1

export WEBOTS_HOME=/opt/webots/webots
export LD_LIBRARY_PATH=$WEBOTS_HOME/lib/webots:$LD_LIBRARY_PATH

export PATH="$PATH:$WEBOTS_HOME"

export GAME_CONTROLLER_HOME=/opt/webots/gamecontroller

